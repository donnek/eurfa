# README #

This repo holds the canonical version of Eurfa, a GPLed Welsh dictionary that is the basis of the [**Eurfa**](http://eurfa.org.uk) online dictionary and [**Autoglosser2**](http://autoglosser.org.uk), a Welsh glosser/tagger.  An auxiliary file, *inflected_verbs.sql*, holds inflected verb-forms for around 4,000 Welsh verbs, but many of these are unlikely to be encountered in modern Welsh.  A further auxiliary file, *multiword.sql* holds entries for phrases or multiword expressions.

The *eurfa.sql* file can be imported directly into PostgreSQL: 
```
createdb -U username eurfa
psql -U username -d eurfa < eurfa.sql
```

If you are not running PostgreSQL, you can use the *.csv* file instead - this will open in a spreadsheet like LibreOffice Calc.

For more information of Eurfa's structure, see Chapter 5 of the [Autoglosser2 manual](http://autoglosser.org.uk/resources/manual.pdf).
