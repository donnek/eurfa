--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: multiword; Type: TABLE; Schema: public; Owner: kevin; Tablespace: 
--

CREATE TABLE multiword (
    id integer NOT NULL,
    surface character varying(100),
    lemma character varying(100),
    enlemma character varying(100),
    clar character varying(250),
    pos character varying(20),
    gender character varying(20),
    number character varying(50),
    tense character varying(100),
    usage character varying(50),
    gramnotes character varying(250),
    extended character varying(250),
    corcencc character varying(50),
    posplus character varying(100),
    deriv character varying(100),
    plural character varying(50)
);


ALTER TABLE public.multiword OWNER TO kevin;

--
-- Name: multiword_id_seq; Type: SEQUENCE; Schema: public; Owner: kevin
--

CREATE SEQUENCE multiword_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.multiword_id_seq OWNER TO kevin;

--
-- Name: multiword_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: kevin
--

ALTER SEQUENCE multiword_id_seq OWNED BY multiword.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: kevin
--

ALTER TABLE ONLY multiword ALTER COLUMN id SET DEFAULT nextval('multiword_id_seq'::regclass);


--
-- Data for Name: multiword; Type: TABLE DATA; Schema: public; Owner: kevin
--

INSERT INTO multiword VALUES (203016, 'fel petai', 'fel petai', 'as it were', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212590, 'ar fin', 'min', 'on the verge', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212591, 'ar gael', 'cael', 'available', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212718, 'lefel A', 'lefel', 'A-level', NULL, 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (212719, 'lefel O', 'lefel', 'O-level', NULL, 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (212722, 'mam yng nghyfraith', 'mam', 'mother in law', NULL, 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (212667, 'o danodd', 'tan', 'below', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212676, 'syth bín', 'syth', 'immediately', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212683, 'ta waeth', 'drwg', 'anyway', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212688, 'wrth gwrs', 'wrth gwrs', 'of course', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212685, 'wir yr', 'gwir', 'honestly', NULL, 'im', NULL, NULL, NULL, NULL, NULL, NULL, 'Ebych', NULL, NULL, '');
INSERT INTO multiword VALUES (212693, 'y fi', 'fi', 'I', NULL, 'pron', NULL, '1s', NULL, NULL, NULL, NULL, 'Rha pers 1 u', NULL, NULL, '');
INSERT INTO multiword VALUES (212695, 'y ni', 'ni', 'we', NULL, 'pron', NULL, '1p', NULL, NULL, NULL, NULL, 'Rha pers 1 ll', NULL, NULL, '');
INSERT INTO multiword VALUES (197826, 'cloc larwm', 'cloc larwm', 'alarm-clock', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198050, 'enwadur cyffredin', 'enwadur cyffredin', 'common denominator', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198137, 'gwestai gwadd', 'gwestai gwadd', 'invited guest', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198176, 'diagram cangen', 'diagram cangen', 'tree diagram', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198253, 'ateb bras', 'ateb bras', 'rough answer', '(estimate)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198371, 'ymadrodd parod', 'ymadrodd parod', 'set phrase', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199034, 'cyfnod gwirio', 'cyfnod gwirio', 'polling interval', '(computer)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199046, 'blwch cyfunol', 'blwch cyfunol', 'combobox', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199480, 'deallusrwydd artiffisial', 'deallusrwydd artiffisial', 'artificial intelligence', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199922, 'rhif trefnol', 'rhif trefnol', 'ordinal number', '(first, second, etc)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200660, 'atalnod llawn', 'atalnod llawn', 'full stop', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200688, 'pôl piniwn', 'pôl piniwn', 'opinion poll', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200755, 'parth enw', 'parth enw', 'namespace', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200985, 'hanner troad', 'hanner troad', 'half turn', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200995, 'arwyneb gwastad', 'arwyneb gwastad', 'flat surface', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200996, 'arwyneb plân', 'arwyneb plân', 'plane surface', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201190, 'achos cryf dros', 'achos cryf dros', 'a strong case for', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201445, 'hollti blew', 'hollti blew', 'splitting hairs', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201604, 'chwarter troad', 'chwarter troad', 'quarter turn', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201614, 'arfer gorau', 'arfer gorau', 'best practice', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201986, 'cyfeiriad IP sefydlog', 'cyfeiriad IP sefydlog', 'static IP address', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (202559, 'graff trawsnewid', 'graff trawsnewid', 'conversion graph', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203448, 'man clymu', 'man clymu', 'mount point', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203449, 'man cychwyn', 'man cychwyn', 'starting point', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203781, 'cwpwrdd cornel', 'cwpwrdd cornel', 'Mozilla sidebar', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204049, 'buanedd cyfartalog', 'buanedd cyfartalog', 'average speed', '(physics)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204143, 'cyfiau cymhlyg', 'cyfiau cymhlyg', 'complex conjugate', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204263, 'bwlch di-dor', 'bwlch di-dor', 'non-breaking space', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204279, 'rhif arnawf', 'rhif arnawf', 'float', '(number)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204280, 'rhif coll', 'rhif coll', 'missing number', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204281, 'rhif cymysg', 'rhif cymysg', 'mixed number', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204465, 'graff gwasgariad', 'graff gwasgariad', 'scatter graph', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204483, 'rhywfath o', 'rhywfath o', 'some kind of', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204586, 'cymesuredd adlewyrchiad', 'cymesuredd adlewyrchiad', 'reflective symmetry', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205581, 'ffactor graddfa', 'ffactor graddfa', 'scale factor', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205814, 'llyfr siec', 'llyfr siec', 'chequebook', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206592, 'troellwr dillad', 'troellwr dillad', 'spin-dryer', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206608, 'cebl cyfechelog', 'cebl cyfechelog', 'coaxial cable', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206835, 'siart cylch', 'siart cylch', 'pie chart', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207593, 'gwirydd sillafu', 'gwirydd sillafu', 'spellchecker', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207711, 'arbedydd sgrin', 'arbedydd sgrin', 'screensaver', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207945, 'treiglad llaes', 'treiglad llaes', 'aspirate mutation', '(eg c -> ch)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207946, 'ymyriad ar breifatrwydd', 'ymyriad ar breifatrwydd', 'invasion of privacy', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208052, 'pryf cop', 'pryf cop', 'spider', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208059, 'ffracsiwn pendrwm', 'ffracsiwn pendrwm', 'improper fraction', '(where the numerator is larger than the denominator)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208092, 'mur gwarchod', 'mur gwarchod', 'firewall', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208513, 'cynllun gweithredu', 'cynllun gweithredu', 'action plan', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208614, 'cylch docyn', 'cylch docyn', 'token ring', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (209182, 'blewyn croes', 'blewyn croes', 'crosshair', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (197530, 'lled breichiau', 'lled breichiau', 'armspan', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (197531, 'lle degol', 'lle degol', 'decimal place', '(maths)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (197640, 'hafaliad llinol', 'hafaliad llinol', 'linear equation', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (197676, 'Thatcheriad rhonc', 'Thatcheriad rhonc', 'unreconstructed Thatcherite', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (197716, 'patrwm ailadrodd', 'patrwm ailadrodd', 'repeating pattern', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198098, 'pen blaen', 'pen blaen', 'frontend', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198132, 'cric cymalau', 'cric cymalau', 'arthritis', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198220, 'cynnal a chadw', 'cynnal a chadw', 'repair and maintenance', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198289, 'amserydd tywod', 'amserydd tywod', 'sand timer', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198341, 'mesur brest', 'mesur brest', 'chest measurement', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198342, 'mesur clun', 'mesur clun', 'hip measurement', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198455, 'enillydd gwobr', 'enillydd gwobr', 'prizewinner', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198576, 'canol nos', 'canol nos', 'midnight', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198645, 'sugnydd llwch', 'sugnydd llwch', 'vacuum cleaner', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198666, 'peiriant twll yn y wal', 'peiriant twll yn y wal', 'cashpoint', '(hole in the wall)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198673, 'gwerth lle', 'gwerth lle', 'place value', '(maths)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198863, 'tâp mesur', 'tâp mesur', 'tape measure', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198871, 'botwm adio', 'botwm adio', 'addition key', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198907, 'manwl gywirdeb', 'manwl gywirdeb', 'accuracy', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199103, 'blwch a blewyn', 'blwch a blewyn', 'box and whisker', '(statistics)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199104, 'blwch anfon', 'blwch anfon', 'outbox', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199110, 'gweithrediad gwrthdro', 'gweithrediad gwrthdro', 'inverse operation', '(maths)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199205, 'lluosrif cyffredin lleiaf', 'lluosrif cyffredin lleiaf', 'lowest common denominator', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199314, 'canol y cymesuredd', 'canol y cymesuredd', 'centre of symmetry', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199350, 'bar noddfa', 'bar noddfa', 'dockbar', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199355, 'negesydd chwim', 'negesydd chwim', 'instant messager', '(program)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199411, 'rhif prifol', 'rhif prifol', 'cardinal number', '(one, two, etc)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199545, 'bara gwenith', 'bara gwenith', 'wheaten bread', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199876, 'glaw mân', 'glaw mân', 'drizzle', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (199964, 'llygad y dydd', 'llygad y dydd', 'daisy', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200077, 'cyfwng dosbarth', 'cyfwng dosbarth', 'class interval', '(maths)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200134, 'niwed straen ailadroddus', 'niwed straen ailadroddus', 'RSI', '(repetitive strain injury)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200141, 'dŵr hallt', 'dŵr hallt', 'brine', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200466, 'enw gwesteiwr', 'enw gwesteiwr', 'hostname', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200490, 'map Arolwg Ordnans', 'map Arolwg Ordnans', 'Ordnance Survey map', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200515, 'ail isradd', 'ail isradd', 'square root', '(maths)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200619, 'papur wal', 'papur wal', 'wallpaper', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201130, 'canlyniad ffafriol', 'canlyniad ffafriol', 'favourable result', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201383, 'troad llawn', 'troad llawn', 'full turn', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201453, 'arwydd hafal', 'arwydd hafal', 'equals sign', '(maths)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201456, 'arwyneb crwm', 'arwyneb crwm', 'curved surface', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201631, 'nodiant indecs', 'nodiant indecs', 'index notation', '(maths)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201689, 'ffôn cellol', 'ffôn cellol', 'cellular phone', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201823, 'copi proflennol', 'copi proflennol', 'proof copy', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201995, 'cyfeirnod grid', 'cyfeirnod grid', 'grid reference', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (202134, 'tabl chwech', 'tabl chwech', 'six-times table', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (202135, 'tabl colyn', 'tabl colyn', 'pivot table', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (202409, 'negesu chwim', 'negesu chwim', 'instant messaging', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (202583, 'golau cylch', 'golau cylch', 'spotlight', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (202757, 'codiad cyflog', 'codiad cyflog', 'pay-rise', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (202933, 'marc rhifo', 'marc rhifo', 'tally mark', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203044, 'safle tirlenwi', 'safle tirlenwi', 'landfill site', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203060, 'cyfrif cyfredol', 'cyfrif cyfredol', 'current account', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203061, 'cyfrif cynilo', 'cyfrif cynilo', 'savings account', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203411, 'cylch bywyd', 'cylch bywyd', 'lifecycle', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203412, 'cylch cythreulig', 'cylch cythreulig', 'vicious circle', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203433, 'llais yn llefain yn y diffeithwch', 'llais yn llefain yn y diffeithwch', 'a voice crying in the wilderness', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203586, 'llythyr cysur', 'llythyr cysur', 'letter of comfort', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204099, 'pen cefn', 'pen cefn', 'backend', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204282, 'rhif cysefin', 'rhif cysefin', 'prime number', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204656, 'degolyn cylchol', 'degolyn cylchol', 'recurring decimal', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (204727, 'tâl sefydlog', 'tâl sefydlog', 'standing charge', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205104, 'adroddiad banc', 'adroddiad banc', 'bank statement', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205335, 'pres poced', 'pres poced', 'pocket money', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205340, 'treiglad trwynol', 'treiglad trwynol', 'nasal mutation', '(eg c -> ngh)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205349, 'canol y cylchdro', 'canol y cylchdro', 'centre of rotation', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205417, 'mynegiad rheolaidd', 'mynegiad rheolaidd', 'regular expression', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205756, 'sylw dyledus', 'sylw dyledus', 'due consideration', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205777, 'ffrwd RSS', 'ffrwd RSS', 'RSS feed', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205851, 'cysawd gweithredu', 'cysawd gweithredu', 'operating system', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (205990, 'fersiwn deuaidd', 'fersiwn deuaidd', 'binary', '(program)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206230, 'darn arian', 'darn arian', 'coin', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206231, 'darn punt', 'darn punt', 'one-pound coin', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206232, 'darpar awdur', 'darpar awdur', 'would-be author', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206349, 'derbyniad gwresog', 'derbyniad gwresog', 'a warm reception', '(welcome)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206381, 'gwasgiad bysell', 'gwasgiad bysell', 'keypress', '(on computer keyboard)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206626, 'treiglad meddal', 'treiglad meddal', 'soft mutation', '(eg c -> g)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206688, 'hanner cylch', 'hanner cylch', 'semi-circle', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206907, 'bara brith', 'bara brith', 'fruit loaf', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206964, 'blwch derbyn', 'blwch derbyn', 'inbox', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (206462, 'bardd llawryfog', 'bardd', 'poet laureate', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207083, 'mesur gwasg', 'mesur gwasg', 'waist measurement', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207245, 'ymadrodd treuliedig', 'ymadrodd treuliedig', 'hackneyed phrase', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207342, 'diogelydd sgrin', 'diogelydd sgrin', 'screen protector', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207730, 'pren mesur', 'pren mesur', 'ruler', '(measurement)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207900, 'metr ciwb', 'metr ciwb', 'cubic metre', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (207901, 'metr sgwâr', 'metr sgwâr', 'square metre', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208103, 'crynswth hanfodol', 'crynswth hanfodol', 'critical mass', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208232, 'dant pwdr', 'dant pwdr', 'bad tooth', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208298, 'cymesuredd cylchdro', 'cymesuredd cylchdro', 'rotational symmetry', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208321, 'amlinelliad dangosol', 'amlinelliad dangosol', 'illustrative outline', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (208404, 'rhediad argraffu', 'rhediad argraffu', 'print run', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (212373, 'bach sgwâr', 'bach', 'square bracket', '( [ ] )', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (212375, 'bach cyrliog', 'bach', 'curly bracket', '( { } ) ', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (212713, 'India roc', 'roc', 'rock', '(confectionery)', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (203567, 'ongl fewnol', 'ongl fewnol', 'interior angle', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (197487, 'arian parod', '', 'cash', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (203310, 'darbodion maint', '', 'economies of scale', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (203364, 'pwyntiau''r cwmpawd', '', 'points of the compass', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (203931, 'arian poced', '', 'pocket money', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (206324, 'ffa pob', '', 'baked beans', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (207456, 'lleoedd degol', 'lle degol', 'decimal places', '(maths)', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (208230, 'arian papur', '', 'banknotes', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (212701, 'gweilch y pysgod', 'gwalch', 'ospreys', NULL, 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (212708, 'pennau gliniau', 'penglin', 'knees', NULL, 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (197366, 'wyth deg', 'wyth deg', 'eighty', '(80)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (197618, 'neilltuol hawdd', 'neilltuol hawdd', 'particularly easy', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (198279, 'naw deg', 'naw deg', 'ninety', '(90)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (198676, 'pedwar deg', 'pedwar deg', 'forty', '(40)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (199129, 'un deg un', 'un deg un', 'eleven', '(11)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (200651, 'un ar ddeg', 'un ar ddeg', 'eleven', '(11)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (201287, 'hanner cant', 'hanner cant', 'fifty', '(50)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (202262, 'cymaint â', 'cymaint â', 'as big as', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (202941, 'ar frys', 'ar frys', 'urgent', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (203695, 'wrth law', 'wrth law', 'to hand', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (203706, 'manwl gywir', 'manwl gywir', 'exact', '(precise)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (204746, 'anymddiheurol o besimistaidd', 'anymddiheurol o besimistaidd', 'unapologetically pessimistic', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (204962, 'tri deg', 'tri deg', 'thirty', '(30)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (205030, 'newydd sbon', 'newydd sbon', 'brand new', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (205326, 'ar werth', 'ar werth', 'for sale', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (206697, 'rhwym o golli', 'rhwym o golli', 'bound to lose', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (206764, 'deg ar hugain', 'deg ar hugain', 'thirty', '(30)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (207033, 'ar wasgar', 'ar wasgar', 'scattered', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (207445, 'plygio byw', 'plygio byw', 'hotplug', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (207670, 'sglein uchel', 'sglein uchel', 'high gloss', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (208637, 'i mewn', 'i mewn', 'incoming', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (209059, 'pum deg', 'pum deg', 'fifty', '(50)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (196951, 'o ddifrif', 'o ddifrif', 'really', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (197010, 'monyn nhw', 'monyn nhw', 'not them', '(spoken)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (197698, 'union gywir', 'union gywir', 'exactly right', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198044, 'monoch chi', 'monoch chi', 'not you', '(pl, spoken)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198086, 'mono ti', 'mono ti', 'not you', '(sg, spoken)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198146, 'mohonot ti', 'mohonot ti', 'not you', '(sg)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198607, 'mohonom ni', 'mohonom ni', 'not us', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198785, 'fel arall', 'fel arall', 'alternatively', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198920, 'i raddau helaeth', 'i raddau helaeth', 'in large measure', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198990, 'mohono fe', 'mohono fe', 'not him', '(South)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (199388, 'mohono i', 'mohono i', 'not me', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200056, 'yn hwyr neu''n hwyrach', 'yn hwyr neu''n hwyrach', 'sooner or later', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200084, 'yn llygad ei le', 'yn llygad ei le', 'absolutely correct', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200369, 'mohono fo', 'mohono fo', 'not him', '(North)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200433, 'fel arfer', 'fel arfer', 'as usual', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200470, 'yn fras', 'yn fras', 'approximately', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200814, 'mono fi', 'mono fi', 'not me', '(spoken)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200919, 'yn ôl', 'yn ôl', 'back', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200927, 'gan amlaf', 'gan amlaf', 'most frequently', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200998, 'ar y goriwaered', 'ar y goriwaered', 'in decline', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (201280, 'ar unrhyw adeg', 'ar unrhyw adeg', 'at any time', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (201378, 'mwy na thebyg', 'mwy na thebyg', 'more than likely', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (201628, 'mohonon ni', 'mohonon ni', 'not us', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (201852, 'mono fe', 'mono fe', 'not him', '(spoken, South)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (201891, 'ar gyfartaledd', 'ar gyfartaledd', 'on average', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (201974, 'ar ddisberod', 'ar ddisberod', 'astray', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (201976, 'ar draul', 'ar draul', 'at the expense of', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202002, 'i''r chwith', 'i''r chwith', 'to the left', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202405, 'naw o''r gloch', 'naw o''r gloch', 'nine o''clock', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202502, 'yn unsain', 'yn unsain', 'in unison', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202515, 'mohonoch chi', 'mohonoch chi', 'not you', '(pl)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202621, 'yn eu gŵydd', 'yn eu gŵydd', 'in their presence', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202668, 'ar adnau', 'ar adnau', 'on deposit', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202796, 'ar goedd', 'ar goedd', 'publicly', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (202851, 'dros dro', 'dros dro', 'for a while', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205119, 'mono fo', 'mono fo', 'not him', '(spoken, North)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205214, 'masnachol ei sail', 'masnachol ei sail', 'on a commercial basis', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205355, 'drwy gydol y nos', 'drwy gydol y nos', 'throughout the night', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205382, 'fesul tudalen', 'fesul tudalen', 'pagewise', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205576, 'yr eiddoch yn gywir', 'yr eiddoch yn gywir', 'yours sincerely', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205383, 'fesul un', 'fesul un', 'each one', '', 'adv', '', '', NULL, NULL, NULL, 'each one, individually', 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (207330, 'unwaith ac am byth', 'unwaith ac am byth', 'once and for all', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (207477, 'fesul llinell', 'fesul llinell', 'linewise', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (207617, 'ar un adeg', 'ar un adeg', 'at one time', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (207875, 'ar hap', 'ar hap', 'at random', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (208352, 'dro ar ôl tro', 'dro ar ôl tro', 'time after time', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (208730, 'yn gyfangwbl', 'yn gyfangwbl', 'entirely', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (208932, 'fel y cyfryw', 'fel y cyfryw', 'as such', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (209015, 'heb os nac onibai', 'heb os nac onibai', 'with no ifs or buts', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (198319, 'dod i rym', 'dod i rym', 'take effect', '', 'v', NULL, NULL, 'infin', NULL, NULL, NULL, 'B e', NULL, NULL, '');
INSERT INTO multiword VALUES (199336, 'gwirio sillafu', 'gwirio sillafu', 'spellcheck', '', 'v', NULL, NULL, 'infin', NULL, NULL, NULL, 'B e', NULL, NULL, '');
INSERT INTO multiword VALUES (200770, 'cael gwared o', 'cael gwared o', 'get rid of', '', 'v', NULL, NULL, 'infin', NULL, NULL, NULL, 'B e', NULL, NULL, '');
INSERT INTO multiword VALUES (199778, 'ar oledd', 'ar oledd', 'sloping', '', 'adv', '', '', NULL, NULL, NULL, 'sloping, on a slope', 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200837, 'ar y gweill', 'ar y gweill', 'in progress', '', 'adv', '', '', NULL, NULL, NULL, 'in progress, underway, on the go', 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (196834, 'nos Wener', 'nos Wener', 'Friday night', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (197378, 'ffeil gofnod', 'ffeil gofnod', 'logfile', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (197476, 'saith deg', 'saith deg', 'seventy', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (197632, 'mynegi pryder', 'mynegi pryder', 'express concern', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (197819, 'pa un ai oedd yn addas ai peidio', 'pa un ai oedd yn addas ai peidio', 'whether it was appropriate or not', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (197688, 'y wladwriaeth les', 'y wladwriaeth les', 'the welfare state', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (198090, 'canslo gwall', 'canslo gwall', 'cancel an error', '(computing)', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (197912, 'trwch blewyn', 'trwch blewyn', 'marginal', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (198221, 'cynnal arolwg', 'cynnal arolwg', 'conduct a survey', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (198495, 'rhwyddhau''r ffordd iddo', 'rhwyddhau''r ffordd iddo', 'ease the way for him', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199010, 'gyda synnwyr trannoeth', 'gyda synnwyr trannoeth', 'with hindsight', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199114, 'rhannu''n union i', 'rhannu''n union i', 'divide exactly into', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199275, 'y cyfryngau torfol', 'y cyfryngau torfol', 'the mass media', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199160, 'cyfradd llog', 'cyfradd llog', 'interest rate', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (199161, 'cymdeithas adeiladu', 'cymdeithas adeiladu', 'building society', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (199380, 'hyd y bo modd', 'hyd y bo modd', 'as far as possible', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199458, 'nid pledio ... mo hyn', 'nid pledio ... mo hyn', 'this is not pleading ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199477, 'ateb yr un diben', 'ateb yr un diben', 'serve the same purpose', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199496, 'cymryd yn ganiataol', 'cymryd yn ganiataol', 'take for granted', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199625, 'rwyf yn amheus yn ei gylch', 'rwyf yn amheus yn ei gylch', 'I''m doubtful about it', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199700, 'cnoi cil ar', 'cnoi cil ar', 'reflect on', '(lit chew the cud on)', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199750, 'mewn perthynas â', 'mewn perthynas â', 'in relation to', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199942, 'os oes perygl fod ...', 'os oes perygl fod ...', 'if there is a danger that ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199986, 'popeth dan haul', 'popeth dan haul', 'absolutely everything', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199834, 'celfyddyd er mwyn celfyddyd', 'celfyddyd er mwyn celfyddyd', 'ars gratia artis (art for art''s sake)', '(art for art''s sake)', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200055, 'yn gymysg â', 'yn gymysg â', 'mixed in with', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (199994, 'hel dail', 'hel dail', 'stall for time', '(lit gather leaves)', 'vh', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200226, 'diau fod ...', 'diau fod ...', 'granted that ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200266, 'os oes dewis', 'os oes dewis', 'if there is a choice', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200189, 'eitha'' debyg', 'eitha'' debyg', '[it is] most likely', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (200321, 'datblygu yn', 'datblygu yn', 'develop into', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200400, 'clirio''r arddangosydd', 'clirio''r arddangosydd', 'clear the display', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200406, 'hynny yw', 'hynny yw', 'that is to say', '(i.e.)', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200439, 'mae ganddo feddwl mawr o ...', 'mae ganddo feddwl mawr o ...', 'he thinks highly of ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (200440, 'mae taer angen arno i wneud hyn', 'mae taer angen arno i wneud hyn', 'there is a pressing need for him to do this', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201062, 'mor fywiog ag erioed', 'mor fywiog ag erioed', 'as lively as ever', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201097, 'iaith arwyddion', 'iaith arwyddion', 'sign language', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201098, 'iaith lafar', 'iaith lafar', 'colloquial speech', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201246, 'cyn belled ag y bo''n ymarferol', 'cyn belled ag y bo''n ymarferol', 'as far as practicable', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201217, 'echelin fertigol', 'echelin fertigol', 'vertical axis', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201218, 'echelin lorwedd', 'echelin lorwedd', 'horizontal axis', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201505, 'gan un nad oes ganddo mo''r amynedd', 'gan un nad oes ganddo mo''r amynedd', 'by someone who no longer has the patience', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201613, 'os mai ... sydd ...', 'os mai ... sydd ...', 'if it''s ... which is ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201667, '10 y cant', '10 y cant', '10 per cent', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201672, 'mai dyma', 'mai dyma', 'that this is', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201790, 'cronfa ddata', 'cronfa ddata', 'database', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201914, 'o ddefnyddio ...', 'o ddefnyddio ...', 'when using ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (201899, 'ffon fesur', 'ffon fesur', 'ruler', '(measurement)', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201999, 'gwerthoedd lle', 'gwerth lle', 'place values', '(maths)', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (202290, 'bron nad awn mor bell â dweud bod ...', 'bron nad awn mor bell â dweud bod ...', 'we might almost go so far as to say that ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (202620, 'yn ei amrywiol weddau', 'yn ei amrywiol weddau', 'in its different guises', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (202787, 'ni fydd raid', 'ni fydd raid', 'there will be no need', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (202826, 'y byd sydd ohoni', 'y byd sydd ohoni', 'the real world', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (203015, 'fel iau ar ei war', 'fel iau ar ei war', 'like a millstone around his neck', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (202996, 'oriau mân', '', 'the early hours', '', 'n', 'f', 'pl', NULL, NULL, NULL, NULL, 'E b ll', NULL, NULL, '');
INSERT INTO multiword VALUES (203025, 'at hynny', 'at hynny', 'besides that', '', 'conj', '', '', NULL, NULL, NULL, NULL, 'Cys cyd', NULL, NULL, '');
INSERT INTO multiword VALUES (203053, 'dim ... na', 'dim ... na', 'neither ... nor', '', 'conj', '', '', NULL, NULL, NULL, NULL, 'Cys cyd', NULL, NULL, '');
INSERT INTO multiword VALUES (203182, 'â''i ben yn ei blu', 'â''i ben yn ei blu', 'with his head in the sand', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (203424, 'cymryd ar', 'cymryd ar', 'pretend', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (203271, 'breichiau ongl', '', 'arms of an angle', '', 'n', 'f', 'pl', NULL, NULL, NULL, NULL, 'E b ll', NULL, NULL, '');
INSERT INTO multiword VALUES (203564, 'ongl aflem', 'ongl aflem', 'obtuse angle', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (203565, 'ongl allanol', 'ongl allanol', 'exterior angle', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (203566, 'ongl atblyg', 'ongl atblyg', 'reflex angle', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (204101, 'penderfyniad di-droi''n ôl', 'penderfyniad di-droi''n ôl', 'a decision from which there is no turning back', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (203932, 'arian tramor', 'arian', 'foreign currency', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (204496, 'byddwch yn gyson eich defnydd ohono', 'byddwch yn gyson eich defnydd ohono', 'be consistent in your use of it', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (204389, 'sawl math', '', 'various kinds', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (204380, 'graddau canradd', '', 'degrees centigrade', '', 'n', 'f', 'pl', NULL, NULL, NULL, NULL, 'E b ll', NULL, NULL, '');
INSERT INTO multiword VALUES (204578, 'o nerth i nerth', 'o nerth i nerth', 'from strength to strength', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (204475, 'arian cywir', '', 'correct change', '', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (204969, 'gosod her', 'gosod her', 'lay down a challenge', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205197, 'ymhellach gweler ...', 'ymhellach gweler ...', 'for more details see ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205270, 'gair i''r gall', 'gair i''r gall', 'a word to the wise', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205339, 'mwy amrywiol o''r hanner na ...', 'mwy amrywiol o''r hanner na ...', 'more varied by far than ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205275, 'ym mhle', 'ym mhle', 'where', '(object of clause)', 'conj', '', '', NULL, NULL, NULL, NULL, 'Cys cyd', NULL, NULL, '');
INSERT INTO multiword VALUES (205406, 'dymunaf ddiolch iddo', 'dymunaf ddiolch iddo', 'I would like to thank him', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205308, 'hunaniaeth genedlaethol', 'hunaniaeth genedlaethol', 'national identity', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (205522, 'erbyn meddwl', 'erbyn meddwl', 'come to think of it', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205572, 'yn y bôn 8', 'yn y bôn 8', 'in octal (base 8)', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205573, 'yn y naill gerdd ar ôl y llall', 'yn y naill gerdd ar ôl y llall', 'in one poem after another', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205577, 'Yr Wyddgrug', 'Yr Wyddgrug', 'Mold', '', 'name', 'f', 'sg', NULL, '', NULL, NULL, 'E p b', 'place', NULL, '');
INSERT INTO multiword VALUES (205683, 'fe ellid dweud bod ...', 'fe ellid dweud bod ...', 'it could be said that ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205591, 'ongl lem', 'ongl lem', 'acute angle', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (205856, 'trafod data', 'trafod data', 'handle data', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205911, 'ymgynghori â', 'ymgynghori â', 'consult with', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206116, 'cadw deupen llinyn ynghyd', 'cadw deupen llinyn ynghyd', 'make both ends meet', '(budget)', 'vh', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206121, 'cael hyd i', 'cael hyd i', 'come across', '(find)', 'vh', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (205941, 'mono i', 'mono i', 'not me', '(spoken)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (206372, 'cyflawni hunanladdiad', 'cyflawni hunanladdiad', 'commit suicide', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206409, 'gwneud newidiadau mân', 'gwneud newidiadau mân', 'finetune', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206498, 'tomen graidd', 'tomen graidd', 'core dump', '(kernel)', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (206636, 'fel rheol', 'fel rheol', 'usually', '(as a rule)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (206867, 'dwyn i''r blaen', 'dwyn i''r blaen', 'bring to the front', '(of a pile)', 'vh', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206921, 'os nad yw yno', 'os nad yw yno', 'if it isn''t there', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206922, 'pe na bai enw''r awdur ...', 'pe na bai enw''r awdur ...', 'if the name of the author were not ...', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206970, 'sy’n amrywio o ran eu perthnasedd', 'sy’n amrywio o ran eu perthnasedd', 'which differ in their relevance', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (207017, 'hidlo drwy', 'hidlo drwy', 'filter on', '(specified criterion)', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (206808, 'swyddfa bost', 'swyddfa bost', 'post office', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (206809, 'swyddfa''r post', 'swyddfa''r post', 'post office', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (206937, 'llinell grom', 'llinell grom', 'curved line', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (207052, 'cynnig a chynnig', 'cynnig a chynnig', 'trial and error', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (207481, 'naill ai ... neu', 'naill ai ... neu', 'either ... or', '', 'conj', '', '', NULL, NULL, NULL, NULL, 'Cys cyd', NULL, NULL, '');
INSERT INTO multiword VALUES (207589, 'llai na', 'llai na', 'smaller than', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (207476, 'fesul gwesteiwr', 'fesul gwesteiwr', 'per host', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (207676, 'losin llygad', '', 'eyecandy', '', 'n', 'f', 'pl', NULL, NULL, NULL, NULL, 'E b ll', NULL, NULL, '');
INSERT INTO multiword VALUES (208055, 'wyddwn i ddim tan hynny', 'wyddwn i ddim tan hynny', 'I did not know until then', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (208086, 'lluniadu wrth raddfa', 'lluniadu wrth raddfa', 'draw to scale', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (208126, 'sefydlu cynsail', 'sefydlu cynsail', 'set a benchmark', '', 'vh', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (208036, 'nos Sadwrn', 'nos Sadwrn', 'Saturday night', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (207923, 'blynyddoedd naid', 'blwyddyn naid', 'leap years', '', 'n', 'f', 'pl', NULL, NULL, NULL, NULL, 'E b ll', NULL, NULL, '');
INSERT INTO multiword VALUES (207940, 'heb siw na miw', 'heb siw na miw', 'without a trace', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (208311, 'troedio''r llwyfan', 'troedio''r llwyfan', 'treading the boards', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (208340, 'codi gwrychyn', 'codi gwrychyn', 'raise the hackles', '', 'vh', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (208289, 'darpariaeth dameidiog', 'darpariaeth dameidiog', 'piecemeal provision', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (208497, 'cyfundrefn ffeiliau', 'cyfundrefn ffeiliau', 'filesystem', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (209064, 'gofyn am', 'gofyn am', 'ask for', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (209031, 'ffon fetr', 'ffon fetr', 'metre rule', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (208882, 'i''r dde', 'i''r dde', 'to the right', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (196617, 'bob amser', 'bob amser', 'always', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (196766, 'mohonyn nhw', 'mohonyn nhw', 'not them', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (196836, 'mohono fi', 'mohono fi', 'not me', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205456, 'gyda''i gilydd', 'cilydd', 'together', '', 'prep+pron', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (203235, 'ymhen amser', 'ymhen amser', 'in time', '(after a unspecified period)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (203324, 'yn ddiymyrraeth', 'yn ddiymyrraeth', 'without interference', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (203325, 'yn dra pherthnasol ...', 'yn dra pherthnasol ...', 'very relevant to ...', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (203407, 'fesul dyfais', 'fesul dyfais', 'per device', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (203408, 'fesul gair', 'fesul gair', 'word by word', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (203656, 'o reidrwydd', 'o reidrwydd', 'of necessity', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (204211, 'yn ddiymdroi', 'yn ddiymdroi', 'with immediate effect', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (204864, 'moni hi', 'moni hi', 'not her', '(spoken)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (204952, 'mohono hi', 'mohono hi', 'not her', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205034, 'wedi''i luosi â', 'wedi''i luosi â', 'multiplied by', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (197557, 'adwy NID', 'adwy NID', 'NOT gate', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (197684, 'llyfryddiaeth ddethol', 'llyfryddiaeth ddethol', 'select bibliography', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (197762, 'adwy NEU', 'adwy NEU', 'OR gate', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (198407, 'llinell ddrych', 'llinell ddrych', 'mirror line', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (198408, 'llinell doredig', 'llinell doredig', 'dotted line', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (198409, 'llinell glo', 'llinell glo', 'concluding line', '(eg of poem)', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (198778, 'nos Lun', 'nos Lun', 'Monday night', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (199121, 'archeb bost', 'archeb bost', 'postal order', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (199202, 'cyfradd cyfnewid', 'cyfradd cyfnewid', 'exchange rate', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (199332, 'tyweirch artiffisial', 'tyweirch artiffisial', 'astroturf', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (199439, 'pobl gyffredin', 'pobl gyffredin', 'ordinary people', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (199698, 'bysell adlam', 'bysell adlam', 'bounce key', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (200590, 'Gweithrediaeth yr Alban', 'Gweithrediaeth yr Alban', 'The Scottish Executive', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (200731, 'nos Sul', 'nos Sul', 'Sunday night', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201633, 'siop stop-cyntaf', 'siop stop-cyntaf', 'one-stop shop', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (201741, 'toreth o wybodaeth', 'toreth o wybodaeth', 'a wealth of information', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (202046, 'nos Fercher', 'nos Fercher', 'Wednesday night', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (202518, 'siawns deg', 'siawns deg', 'even chance', '(evens)', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (202519, 'siawns wael', 'siawns wael', 'poor chance', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (203580, 'berf anghyflawn', 'berf anghyflawn', 'transitive verb', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (203582, 'berf gyflawn', 'berf gyflawn', 'intransitive verb', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (203609, 'siec teithio', 'siec teithio', 'traveller''s cheque', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (204193, 'basged grog', 'basged grog', 'hanging basket', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (204415, 'nos Fawrth', 'nos Fawrth', 'Tuesday night', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (204749, 'cyflog sylfaenol', 'cyflog sylfaenol', 'basic pay', '', 'n', 'mf', 'sg', NULL, NULL, NULL, NULL, 'E gb u', NULL, NULL, '');
INSERT INTO multiword VALUES (205342, 'Treth Ar Werth', 'Treth Ar Werth', 'Value-Added Tax', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (205344, 'treth incwm', 'treth incwm', 'income tax', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (205570, 'nos Iau', 'nos Iau', 'Thursday night', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (206382, 'gwasgiad llygoden', 'gwasgiad llygoden', 'mouse press', '(on computer)', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (206967, 'blwyddyn naid', 'blwyddyn naid', 'leap year', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (207084, 'olwyn fesur', 'olwyn fesur', 'measuring wheel', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (207343, 'llinell sylfaen', 'llinell sylfaen', 'baseline', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (207546, 'siawns dda', 'siawns dda', 'good chance', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (208110, 'y farchnad rydd', 'y farchnad rydd', 'the free market', '', 'n', 'f', 'sg', NULL, NULL, NULL, NULL, 'E b u', NULL, NULL, '');
INSERT INTO multiword VALUES (212621, 'myn diân i', 'diawl', 'heck!', NULL, 'im', NULL, NULL, NULL, NULL, NULL, NULL, 'Ebych', NULL, NULL, '');
INSERT INTO multiword VALUES (200796, 'yn nhrefn yr wyddor', 'yn nhrefn yr wyddor', 'alphabetical', '', 'h', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (207191, 'lledrau y gwefusau', 'lledr', 'gums', '(mouth)', 'n', 'm', 'pl', NULL, NULL, NULL, NULL, 'E g ll', NULL, NULL, '');
INSERT INTO multiword VALUES (212614, 'hyd yn hyn', 'hyd', 'so far', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, 'so far, up until now', 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212589, 'ar bwys', 'pwys', 'near', NULL, 'prep', NULL, NULL, NULL, NULL, NULL, NULL, 'Ar sym', NULL, NULL, '');
INSERT INTO multiword VALUES (212620, 'i''w gilydd', 'cilydd', 'to each other', NULL, 'prep+pron', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (212666, 'o dano', 'tan', 'under him', NULL, 'prep+pron', 'm', '3s', NULL, 'spoken', NULL, NULL, 'Ar 3 g u', NULL, NULL, '');
INSERT INTO multiword VALUES (212697, 'Canol Oesoedd', 'oes', 'Middle Ages', NULL, 'n', NULL, 'pl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '');
INSERT INTO multiword VALUES (212665, 'o dani', 'tan', 'under her', NULL, 'prep+pron', 'f', '3s', NULL, 'spoken', NULL, NULL, 'Ar 3 b u', NULL, NULL, '');
INSERT INTO multiword VALUES (212672, 'o danon', 'tan', 'under us', NULL, 'prep+pron', NULL, '1p', NULL, 'spoken', NULL, NULL, 'Ar 1 ll', NULL, NULL, '');
INSERT INTO multiword VALUES (212694, 'y nhw', 'nhw', 'they', NULL, 'pron', NULL, '3p', NULL, NULL, NULL, NULL, 'Rha pers 3 ll', NULL, NULL, '');
INSERT INTO multiword VALUES (213479, 'syth bin', 'syth', 'immediately', NULL, 'adv', NULL, NULL, NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (212691, 'y chdi', 'ti', 'you', NULL, 'pron', NULL, '2s', NULL, NULL, NULL, NULL, 'Rha pers 2 u', NULL, NULL, '');
INSERT INTO multiword VALUES (200109, 'gwyriad safonol', 'gwyriad safonol', 'standard deviation', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (198997, 'gwerthwr gorau', 'gwerthwr gorau', 'bestseller', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (200195, 'canol dydd', 'canol dydd', 'midday', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (201684, 'cerdyn credyd', 'cerdyn credyd', 'credit card', '', 'n', 'm', 'sg', NULL, NULL, NULL, NULL, 'E g u', NULL, NULL, '');
INSERT INTO multiword VALUES (212622, 'naill ai', 'naill', 'either', NULL, 'conj', NULL, NULL, NULL, NULL, NULL, NULL, 'Cys cyd', NULL, NULL, '');
INSERT INTO multiword VALUES (205173, 'argraffadwy dyfynedig', 'argraffadwy dyfynedig', 'quoted printable', '', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (206074, 'pedwar ugain', 'pedwar ugain', 'eighty', '(80)', 'adj', '', '', NULL, NULL, NULL, NULL, 'Ans cad u', NULL, NULL, '');
INSERT INTO multiword VALUES (205932, 'ar eu liwt eu hunain', 'ar eu liwt eu hunain', 'on their own account', '(by themselves, lit. on their own lute)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (205942, 'monon ni', 'monon ni', 'not us', '(spoken)', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (206901, 'trwy gydol y drafodaeth', 'trwy gydol y drafodaeth', 'throughout the discussion', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (206962, 'slawer dydd', 'slawer dydd', 'in the old days', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (207099, 'ym mhobman', 'ym mhobman', 'everywhere', '', 'adv', '', '', NULL, NULL, NULL, NULL, 'Adf', NULL, NULL, '');
INSERT INTO multiword VALUES (1, 'titw''r wern', NULL, 'marsh tit', NULL, 'n', 'mf', 'sg', NULL, NULL, NULL, 'marsh tit (Parus palustris)', 'E gb u', NULL, NULL, NULL);
INSERT INTO multiword VALUES (2, 'ysgrech y coed', NULL, 'jay', NULL, 'n', 'f', 'sg', NULL, NULL, NULL, 'jay (Garrulus glandarius)', NULL, NULL, NULL, NULL);


--
-- Name: multiword_id_seq; Type: SEQUENCE SET; Schema: public; Owner: kevin
--

SELECT pg_catalog.setval('multiword_id_seq', 2, true);


--
-- Name: multiword_pk; Type: CONSTRAINT; Schema: public; Owner: kevin; Tablespace: 
--

ALTER TABLE ONLY multiword
    ADD CONSTRAINT multiword_pk PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

